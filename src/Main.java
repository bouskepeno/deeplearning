import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static  void main(String[] args){

        //Input: un matrice des entiers (1,0),(ex: 5x4) pour presenter l'alphabet d'entre
        int [][] matrice = new int [5][4];

        //Reconnaitre la lettre A par exemple
        matrice = new int[][]  {{1, 1, 1,  1},
                                {1, 0, 0,  1},
                                {1, 1, 1,  1},
                                {1, 0, 0,  1},
                                {1, 0, 0,  1}};

        Alphabet alphabet = new Alphabet(5,4,matrice);
        System.out.println("******** Alphabet d'entre ***********");
        alphabet.affiche();

        // 5 x 4 = 20 neurones
        // Etape 1: Inialiser la matrice des poids avec des valeurs aleatoirs (ex matrice de: 15 x 15 )

        System.out.println("*************  matrice iniale des poids *************");
        List<List<Float>> poids;

        //matrice 15*15 generer aleatoirement
        poids = Seed.getRandomMatrice(15,15);
        int c=0;

        //Boucle pour stabiliser la matrice des poids
        while (c<10){
            //couche numero c
            for (int i=0;i<alphabet.getC();i++){
                System.out.println("** neurone numero "+i+" **");
                //Etape 2:  calcule de l'erreur pour le neurone i , colonne i  du matrice des poids
                List<Float> p = new ArrayList<>();
                for (List<Float> l : poids){
                    p.add(l.get(i));
                }
                List g = new ArrayList<Float>();
                Neuron n0 = new Neuron(p);
               System.out.println("Forward:"+n0.Forward(alphabet.getList()));
               System.out.println("Loss: "+n0.Loss(alphabet.sommeList(alphabet.getList()),n0.Forward(alphabet.getList())));

               //Calcule du gardient pour le neurone i de la couche c
                for (Float in : alphabet.getList()) {
                    g.add(n0.Gradient(in, n0.Forward(alphabet.getList()), alphabet.sommeList(alphabet.getList())));
                }
                //trainer le neurone i de la couche c
                n0.train(g);
                //Modifier la colonne i du matrice des poids avec les nouvelles poids apres le train
                poids.set(i,n0.weights);
            }
            System.out.println("************* nouvelle matrice des poids pour la couche "+c+" *************");
            System.out.println(poids);

            //Passer a la couche suivante
            c++;
        }

    }
}
