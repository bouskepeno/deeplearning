import java.util.ArrayList;
import java.util.List;

public class Neuron {
    List<Float> weights;


    public Neuron(List<Float> poids) {
        this.weights = new ArrayList<>();
        this.weights = poids;
    }

    public List<Float> getWeights() {
        return weights;
    }

    public void setWeights(List<Float> weights) {
        this.weights = weights;
    }

    public Float Forward(List<Float> inputs){
        List <Float> result  =new ArrayList<>();
        int i=0;
        float sum = 0;
        for (Float in : this.weights) {
            result.add(inputs.get(i) * this.weights.get(i));
            i++;
        }
        sum = (float) result.stream().mapToDouble(f -> f.floatValue()).sum();
        return sum;
    }

    public Float Loss(Float result, Float expected){
        return (expected - result) *  (expected - result);
    }

    public Float Gradient(Float in,Float result, Float expected){
        return  in * (float)2 * (expected - result);
    }
    public void train( List<Float> g){
        for (int i=0;i<this.weights.size();i++) {
            Float nw = this.weights.get(i) - g.get(i)*(float)0.0000001;
            this.weights.set(i,nw);
        }
    }
}
