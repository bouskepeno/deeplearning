import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Seed {

    public static  float genererf(){
        return new Random().nextFloat();
    }

    public static List getRandomVector(int size){
      List l = new ArrayList<Float>();
       for (int i=0; i<size; i++){
           l.add(genererf());
       }
        return l;
    }

    public static List<List<Float>> getRandomMatrice(int width, int height) {
        List<List<Float>> m = new ArrayList<>();
        for (int i=0; i<height; i++){
            m.add(getRandomVector(width));
        }
        return m;
    }

    public static  List<Float>  getSumResult(List<List<Float>> m){
        List<Float> r = new ArrayList<Float>();
        for (List<Float> inp:m) {
            float totalevent = (float) inp.stream().mapToDouble(f -> f.floatValue()).sum();
            r.add(totalevent);
        }
        return  r;
    }

}
