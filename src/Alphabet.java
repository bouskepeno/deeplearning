import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Alphabet {
    private int l;
    private int c;
    private int [][] matrice;

    public Alphabet(int l, int c, int[][] matrice) {
        this.l = l;
        this.c = c;
        this.matrice = matrice;
    }

    public int getL() {
        return l;
    }

    public void setL(int l) {
        this.l = l;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public int[][] getMatrice() {
        return matrice;
    }

    public void setMatrice(int[][] matrice) {
        this.matrice = matrice;
    }

    public void affiche() {
       for (int i=0;i<this.l;i++){
           for (int j=0;j<this.c;j++){
                System.out.print(matrice[i][j]+" ");
           }
            System.out.println("");
       }
    }
    public  int  getCaseValue(int i, int j){
        return this.matrice[i][j];
    }
    public List<Float> getList(){
        List<Float> l = new ArrayList<>();
        for (int i=0;i<this.l;i++){
            for (int j=0;j<this.c;j++) {
                l.add((float) this.matrice[i][j]);
            }
        }
        return l;
    }

    public  float sommeList(List<Float> l){
        float sm=0;
        for (int i=0;i<l.size();i++){
            sm=sm+l.get(i);
        }
        return sm;
    }
}
